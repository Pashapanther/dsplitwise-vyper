pragma solidity >=0.4.21 <0.6.0;

contract SolTestDynamic {
  int[] public array;
  int sum;

  constructor() public {

  }

  function set_array(int[] memory _array) public {
    array = _array;
  }

  function loop(uint length) public {
    for (uint i = 0; i < length; i++) {
      if ( i % 2 == 0) {
        sum++;
      }
    }
  }
}
