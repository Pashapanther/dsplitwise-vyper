struct Struct:
  a: int128
  b: uint256

test_struct: public(Struct)

array4: public(int128[4])
array16: public(int128[16])
array64: public(int128[64])
array256: public(int128[256])

test_map: public(map(int128, int128))

sum128: public(int128)
sum256: public(int128)
sum512: public(int128)


@public
def __init__():
  pass

@public
def set_array4(_array: int128[4]):
  self.array4 = _array

@public
def set_array16(_array: int128[16]):
  self.array16 = _array

@public
def set_array64(_array: int128[64]):
  self.array64 = _array

@public
def set_array256(_array: int128[256]):
  self.array256 = _array

@public
def loop_128():
  for i in range(128):
    if i % 2 == 0:
      self.sum128 += 1

@public
def loop_256():
  for i in range(256):
    if i % 2 == 0:
      self.sum256 += 1

@public
def loop_512():
  for i in range(512):
    if i % 2 == 0:
      self.sum512 += 1

@public
def add_map(key: int128, value: int128):
  self.test_map[key] = value

@public
def set_struct(_a: int128, _b: uint256):
  self.test_struct = Struct({a: _a, b: _b})
