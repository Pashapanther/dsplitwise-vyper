# DSplitwise.

#=================== structs =====================#

# enum {DEFAULT, ACCEPTED, PENDING}
# bill_state = [0, 1, 2]

# enum {DEFAULT, UNSETTLED, SETTLED, CANCELED}
# dispute_state = [0, 1, 2, 3]

struct Bill:
    description:        string[50]
    lender_addr:        address
    unaccepted_count:   int128
    members_count:      int128
    borrowers:          address[4]
    debts:              wei_value[4]
    states:             int128[4]


struct Dispute:
    plaintiff:          address
    defendant:          address
    creation_time:      timestamp
    total_amount:       wei_value
    state:              int128
    bills_count:        int128
    bill_IDs:           int128[32]

#============== storage variables ================#
debts: public(map(address, wei_value))
holds: public(map(address, wei_value))

bills: public(map(int128, Bill))
bills_count: public(int128)

disputes: public(map(int128, Dispute))
disputes_count: public(int128)

timeout: timedelta
double_state: public(map(address, map(address, int128)))

@public
def __init__():
    self.bills_count = 0
    self.disputes_count = 0
    self.timeout = 3600 * 24 * 365

@public
@payable
def add_balance():
    self.holds[msg.sender] += msg.value

@public
def add_bill(_description: string[50], _borrowers: address[4],
             _debts: wei_value[4], _members_count: int128):

    assert 0 < _members_count and _members_count <= 4 # Invalid member count

    states: int128[4]

    for i in range(4):
        if i < _members_count:
            assert msg.sender != _borrowers[i] # cannot include owner in borrowers
            assert self.holds[_borrowers[i]] >= self.debts[_borrowers[i]] + _debts[i] #not enough funds to create the bill
            states[i] = 2 # pending

    # 0 is an invalid bill ID, cause the default value for 32 bills_IDs is zero, wtf?
    self.bills_count += 1
    self.bills[self.bills_count] = Bill({description: _description,
                                lender_addr: msg.sender,
                                unaccepted_count: _members_count,
                                members_count: _members_count,
                                borrowers: _borrowers,
                                debts: _debts,
                                states: states
                                })


@public
def accept_bill(bill_ID: int128):
    assert 0 < bill_ID and bill_ID <= self.bills_count # invalid bill ID

    bill: Bill = self.bills[bill_ID]

    for i in range(4):
        if i < bill.members_count:
            if bill.borrowers[i] == msg.sender:
                assert bill.states[i] == 2 # pending
                assert self.holds[msg.sender] > self.debts[msg.sender] + bill.debts[i] # not enough funds
                self.bills[bill_ID].states[i] = 1 # accepted
                self.debts[msg.sender] += bill.debts[i]
                self.bills[bill_ID].unaccepted_count -= 1


@public
def get_borrower_id(bill_id: int128, borrower: address) -> int128:
    bill: Bill = self.bills[bill_id]
    for i in range(4):
        if i < bill.members_count:
            if bill.borrowers[i] == borrower:
                return i
    return -1

@public
def file_dispute(bill_IDs: int128[32], bill_count: int128, defendant: address):
    assert self.double_state[msg.sender][defendant] != 1 # can't open a dispute against defendant twice # unsetteled
    assert 0 < bill_count and bill_count <= 32 # invalid bill_count
    total_cred: wei_value
    total_debt: wei_value

    ID: int128
    bill: Bill

    for i in range(32):
        if i < bill_count:
            ID = bill_IDs[i]
            assert 0 < ID and ID <= self.bills_count # invalid bill ID included.
            bill = self.bills[ID]
            assert bill.unaccepted_count == 0 # Unaccpeted bill included.
            if bill.lender_addr == msg.sender and self.get_borrower_id(ID, defendant) != -1: # accepted
                total_cred += bill.debts[self.get_borrower_id(ID, defendant)]
            elif bill.lender_addr == defendant and self.get_borrower_id(ID, msg.sender) != -1: # accepted
                total_debt += bill.debts[self.get_borrower_id(ID, msg.sender)]
            else:
                return

    assert total_cred > total_debt # debts ammount for dispute is not positive
    self.disputes_count += 1
    self.disputes[self.disputes_count] = Dispute({plaintiff: msg.sender,
                                                  defendant: defendant,
                                                  creation_time: block.timestamp,
                                                  total_amount: total_cred - total_debt,
                                                  state: 1, # unsettled,
                                                  bills_count: bill_count,
                                                  bill_IDs: bill_IDs
                                                  })

    self.double_state[msg.sender][defendant] = 1 # unsetteled

@public
def claim_fraud(dispute_ID: int128, bill_ID: int128):
    assert 0 < bill_ID and bill_ID <= self.bills_count
    assert 0 < dispute_ID and dispute_ID <= self.disputes_count

    bill: Bill = self.bills[bill_ID]
    dispute: Dispute = self.disputes[dispute_ID]

    assert dispute.state == 1 # unsetteled

    assert bill.unaccepted_count == 0 # unaccepted bill is not valid.

    for i in range(32):
        if i < dispute.bills_count:
            if dispute.bill_IDs[i] == bill_ID:
                return

    if dispute.plaintiff == bill.lender_addr and self.get_borrower_id(bill_ID, msg.sender) != -1:
        self.disputes[dispute_ID].state = 3 # canceled
    elif dispute.plaintiff == msg.sender and self.get_borrower_id(bill_ID, bill.lender_addr) != -1:
        self.disputes[dispute_ID].state = 3 # canceled
    else:
        return
    self.double_state[dispute.plaintiff][dispute.defendant] = 3 # canceled

@public
def settle_dispute(dispute_ID: int128):
    assert 0 < dispute_ID and dispute_ID <= self.disputes_count
    assert msg.sender == self.disputes[dispute_ID].plaintiff
    assert self.disputes[dispute_ID].creation_time + self.timeout < block.timestamp
    assert self.disputes[dispute_ID].state == 1

    borrower: address[1]
    borrower[0] = msg.sender

    self.disputes[dispute_ID].state = 2 # settled

    self.double_state[msg.sender][self.disputes[dispute_ID].defendant] = 2 # settled

    _borrowers: address[4]
    _borrowers[0] = msg.sender
    _debts: wei_value[4]
    _debts[0] = self.disputes[dispute_ID].total_amount
    _states: int128[4]
    _states[0] = 1

    self.bills_count += 1
    self.bills[self.bills_count] = Bill({description: "forced settlement",
                         lender_addr: self.disputes[dispute_ID].defendant,
                         unaccepted_count: 0,
                         members_count: 1,
                         borrowers: _borrowers,
                         debts: _debts,
                         states: _states
                         })
    self.holds[msg.sender] += self.disputes[dispute_ID].total_amount
    self.holds[self.disputes[dispute_ID].defendant] -= self.disputes[dispute_ID].total_amount
    self.debts[self.disputes[dispute_ID].defendant] -= self.disputes[dispute_ID].total_amount

@public
def withdraw():
   assert self.holds[msg.sender] > self.debts[msg.sender]
   pending_amount: wei_value = self.holds[msg.sender] - self.debts[msg.sender]
   self.holds[msg.sender] = self.debts[msg.sender]
   send(msg.sender, pending_amount)
