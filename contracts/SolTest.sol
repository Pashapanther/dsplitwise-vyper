pragma solidity >=0.4.21 <0.6.0;

contract SolTest {
  struct Struct{
    int a;
    uint b;
  }

  int[4] public array4;
  int[16] public array16;
  int[64] public array64;
  int[256] public array256;

  mapping (int => int) public test_map;

  int public sum128;
  int public sum256;
  int public sum512;

  Struct public test_struct;


  constructor() public {

  }

  function set_array4(int[4] memory _array) public {
    array4 = _array;
  }

  function set_array16(int[16] memory _array) public {
    array16 = _array;
  }

  function set_array64(int[64] memory _array) public {
    array64 = _array;
  }

  function set_array256(int[256] memory _array) public {
    array256 = _array;
  }

  function loop_128() public {
    for (uint i = 0; i < 128; i++) {
      if ( i % 2 == 0) {
        sum128++;
      }
    }
  }

  function loop_256() public {
    for (uint i = 0; i < 256; i++) {
      if ( i % 2 == 0) {
        sum256++;
      }
    }
  }

  function loop_512() public {
    for (uint i = 0; i < 512; i++) {
      if ( i % 2 == 0) {
        sum512++;
      }
    }
  }

  function add_map(int key, int value) public {
    test_map[key] = value;
  }

  function set_struct(int _a, uint _b) public {
    test_struct = Struct(_a, _b);
  }

}
