pragma solidity >=0.4.21 <0.6.0;


contract DSplitwiseStatic {


// DSplitwise.

// =================== structs ===================== //

// enum {DEFAULT, ACCEPTED, PENDING}
// bill_state = [0, 1, 2]

// enum {DEFAULT, UNSETTLED, SETTLED, CANCELED}
// dispute_state = [0, 1, 2, 3]

    struct Bill {
        address lender_addr;
        int128 unaccepted_count;
        int128 members_count;
        string description;
        address[4] borrowers;
        uint256[4] debts;
        int128[4] states;
    }

    struct Dispute {
        address plaintiff;
        address defendant;
        uint256 creation_time;
        uint256 total_amount;
        int128 state;
        int128 bills_count;
        int128[32] bill_IDs;
    }

    event Withdraw(address addr, uint amount);
// ============== storage variables ================ //
    mapping(address => uint256) public debts;
    mapping(address => uint256) public holds;

    mapping(int128 => Bill) public bills;
    int128 public bills_count;

    mapping(int128 => Dispute) public disputes;
    int128 public disputes_count;

    uint256 timeout;
    mapping(address => mapping(address => int128)) double_state;



    constructor() public {
        bills_count = 0;
        disputes_count = 0;
        timeout = 3600 * 24 * 24;
    }

    function get_balance(address addr) public view returns (uint256) {
        return holds[addr] - debts[addr];
    }

    function get_borrower_state(int128 bill_ID, int128 borrower_ID) public view returns (int128){
        return bills[bill_ID].states[uint(borrower_ID)];
    }

    function get_borrower_debt(int128 bill_ID, int128 borrower_ID) public view returns (uint256){
        return bills[bill_ID].debts[uint(borrower_ID)];
    }

    function add_balance() public payable {
        holds[msg.sender] += msg.value;
    }

    function add_bill(string memory _description, address[4] memory _borrowers, uint256[4] memory _debts, int128 _members_count) public {
        assert(0 < _members_count && _members_count <= 4); // Invalid member count

        int128[4] memory states;
        for (uint128 i = 0; i < 4; i++) {
            if (i < uint(_members_count)) {
                assert(msg.sender != _borrowers[i]); // cannot include owner in borrowers
                assert(holds[_borrowers[i]] >= debts[_borrowers[i]] + _debts[i]); // not enough funds to create the bill
                states[i] = 2; // pending
            }
        }

        // 0 is an invalid bill ID, cause the default value for 32 bills_IDs is zero, wtf
        bills_count += 1;
        bills[bills_count] = Bill(
                                  msg.sender,
                                  _members_count,
                                  _members_count,
                                  _description,
                                  _borrowers,
                                  _debts,
                                  states);
    }

    function accept_bill(int128 bill_ID) public {
        assert(0 < bill_ID && bill_ID <= bills_count); // Invalid bill ID

        Bill storage bill = bills[bill_ID];

        for (uint128 i = 0; i < 4; i++) {
            if (i < uint(bill.members_count)) {
                if (bill.borrowers[i] == msg.sender) {
                    assert(bill.states[i] == 2); // pending
                    assert(holds[msg.sender] > debts[msg.sender] + bill.debts[i]);// not enough funds
                    bills[bill_ID].states[i] = 1; // accepted
                    debts[msg.sender] += bill.debts[i];
                    bills[bill_ID].unaccepted_count -= 1;
                }
            }
        }
    }

    function get_borrower_id(int128 bill_id, address borrower) public view returns (int128) {
        Bill storage bill = bills[bill_id];
        for (uint128 i = 0; i < 4; i++) {
            if (i < uint(bill.members_count)) {
                if (bill.borrowers[i] == borrower)
                    return int128(i);
            }
        }
        return -1;
    }

    function file_dispute (int128[32] memory bill_IDs, int128 bill_count, address defendant) public {
        require(double_state[msg.sender][defendant] != 1,"can't open a dispute against defendant");
        require(0 < bill_count && bill_count <= 32, "invalid bill_count");
        uint256 total_cred;
        uint256 total_debt;

        // int128 ID;
        // Bill storage bill;

        for (uint128 i = 0; i < 32; i++) {
            if (i < uint128(bill_count)) {
                int128 ID = bill_IDs[i];
                require(0 < ID && ID <= bills_count, "invalid bill ID included");
                Bill storage bill = bills[ID];
                require(bill.unaccepted_count == 0, "Unaccepted bill included");
                if (bill.lender_addr == msg.sender && get_borrower_id(ID, defendant) != -1) { // accepted
                    total_cred += bill.debts[uint(get_borrower_id(ID, defendant))];
                } else if (bill.lender_addr == defendant && get_borrower_id(ID, msg.sender) != -1) { //accepted
                    total_debt += bill.debts[uint(get_borrower_id(ID, msg.sender))];
                } else {
                    /* require(1 == 0); */
                }
            }
        }

        require(total_cred > total_debt, "debts amount for is not positive");
        disputes_count += 1;
        disputes[disputes_count] = Dispute(msg.sender,
                                           defendant,
                                           block.timestamp,
                                           total_cred - total_debt,
                                           1, // unsetteled
                                           bill_count,
                                           bill_IDs);

        double_state[msg.sender][defendant] = 1; // unsettled
    }

    function claim_fraud(int128 dispute_ID, int128 bill_ID) public {
        assert(0 < bill_ID && bill_ID <= bills_count);
        assert(0 < dispute_ID && dispute_ID <= disputes_count);

        Bill storage bill = bills[bill_ID];
        Dispute storage dispute = disputes[dispute_ID];

        assert(dispute.state == 1); // unsettled
        assert(bill.unaccepted_count == 0); //unaccepted bill is not valid

        for (uint128 i = 0; i < 32; i++) {
            if (i < uint(dispute.bills_count)) {
                if (dispute.bill_IDs[i] == bill_ID) {
                    return;
                }
            }
        }

        if (dispute.plaintiff == bill.lender_addr && get_borrower_id(bill_ID, msg.sender) != -1) {
            disputes[dispute_ID].state = 3; // canceled
        } else if (dispute.plaintiff == msg.sender && get_borrower_id(bill_ID, bill.lender_addr) != -1) {
            disputes[dispute_ID].state = 3; // canceled
        } else {
            return;
        }
        double_state[dispute.plaintiff][dispute.defendant] = 3; // canceled
    }

    function settle_dispute(int128 dispute_ID) public {
        assert(0 < dispute_ID && dispute_ID <= disputes_count);
        assert(msg.sender == disputes[dispute_ID].plaintiff);
        assert(disputes[dispute_ID].creation_time + timeout < block.timestamp);
        assert(disputes[dispute_ID].state == 1);

        address[1] memory borrower;
        borrower[0] = msg.sender;

        disputes[dispute_ID].state = 2; // settled

        double_state[msg.sender][disputes[dispute_ID].defendant] = 2; // settled

        address[4] memory _borrowers;
        _borrowers[0] = msg.sender;
        uint256[4] memory _debts;
        _debts[0] = disputes[dispute_ID].total_amount;
        int128[4] memory _states;
        _states[0] = 1;
        bills_count += 1;
        string memory _descreption;
        _descreption = "forced settlement";
        /* assembly {
            _descreption := mload(add("forced settlement", 50))
        } */
        bills[bills_count] = Bill(
                                  disputes[dispute_ID].defendant,
                                  0,
                                  1,
                                  _descreption,
                                  _borrowers,
                                  _debts,
                                  _states);
        holds[msg.sender] += disputes[dispute_ID].total_amount;
        holds[disputes[dispute_ID].defendant] -= disputes[dispute_ID].total_amount;

        debts[disputes[dispute_ID].defendant] -= disputes[dispute_ID].total_amount;
    }

    function withdraw() public {
        assert(holds[msg.sender] > debts[msg.sender]);
        uint256 pending_amount = uint256(holds[msg.sender] - debts[msg.sender]);
        holds[msg.sender] = debts[msg.sender];
        assert(msg.sender.send(pending_amount));
        emit Withdraw(msg.sender, pending_amount);

    }
}
