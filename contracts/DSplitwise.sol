pragma solidity ^0.5.0;

contract DSplitwise {

    //=================== structs =====================//
    enum BillState {DEFAULT, ACCEPTED, PENDING}
    enum DisputeState {DEFAULT, UNSETTLED, SETTLED, CANCELED}

    struct Bill {
        string description;
        address LenderAddr;
        uint unaccpetedCount;

        address[] borrowers;
        mapping(address => uint) debtAmounts;
        mapping(address => BillState) states;

    }

    struct Dispute {
        address accuser;
        address defendant;
        uint createdAt;
        uint totalAmount;
        DisputeState state;
        uint[] billsIDs;
    }

    //============== storage variables ================//
    mapping (address => uint) public membersDebtBalances;
    mapping (address => uint) public membersHoldMoney;
    uint public billsCount;
    mapping (uint => Bill) public bills;
    uint public disputesCount;
    mapping (uint => Dispute) public disputes;
    uint timeout;
    mapping (address => mapping(address => DisputeState)) double_state;

    //================== modifiers ===================//
    modifier validBill(uint _billID) { require(_billID >= 0 && _billID < billsCount, "invalid bill ID"); _; }

    modifier validDispute(uint _disputeID) {
        require(_disputeID >= 0 && _disputeID < disputesCount, "invalid dispute ID");
        require(disputes[_disputeID].state == DisputeState.UNSETTLED, "invalid (canceled/settled) dispute");
        _;}

    event Withdraw(address addr, uint amount);
    event BalanceChanged(address addr, uint amount);
    //================= constructor ==================//
    constructor() public {
        billsCount = 0;
        disputesCount = 0;
        timeout = 10 seconds;
    }


    //=============== getter functions ===============//
    function getBalance(address _addr) public view returns (uint) {
        return membersHoldMoney[_addr] - membersDebtBalances[_addr];
    }


    function getBillBorrowerState(uint _id, address _addr) validBill(_id) public view returns (BillState) {
        return bills[_id].states[_addr];
    }

    function getBillBorrowerDebt(uint _id, address _addr) validBill(_id) public view returns (uint) {
        return bills[_id].debtAmounts[_addr];
    }

    function getBillBorrowers(uint _id) validBill(_id) public view returns (address[] memory) {
        return bills[_id].borrowers;
    }

    function getDisputeBills(uint _id) validDispute(_id) public view returns (uint[] memory) {
        return disputes[_id].billsIDs;
    }

    // function getDisbutesBills(uint _disputeID) external view validDispute(_disputeID) returns (uint[] memory) {
    //     uint[] memory _result;
    //     for (uint i = 0; i< disputes[_disputeID].billsIDs.length; i++)
    //         _result[i] = disputes[_disputeID].billsIDs[i];
    //     return _result;
    // }


    //============ state changer functions ===========//
    function addBalance() payable public {
        membersHoldMoney[msg.sender] += msg.value;
        emit BalanceChanged(msg.sender, membersHoldMoney[msg.sender]);
    }


    function createBill(string memory _desc, address[] memory _borrowers, uint[] memory _debtAmounts) public returns (uint billID) {

        require(_borrowers.length == _debtAmounts.length);

        billID = billsCount++;
        bills[billID] = Bill(_desc, msg.sender, _borrowers.length, _borrowers);

        for (uint i = 0; i < _borrowers.length; i++) {
            require(membersHoldMoney[_borrowers[i]]  >= membersDebtBalances[_borrowers[i]] + _debtAmounts[i], "not enough fund for join to bill");
            bills[billID].states[_borrowers[i]] = BillState.PENDING;
            bills[billID].debtAmounts[_borrowers[i]] = _debtAmounts[i];
        }
    }

    function acceptBill(uint _billID) validBill(_billID) public {
        require(bills[_billID].states[msg.sender] == BillState.PENDING, "wrong bill ID or already accepted");
        require(membersHoldMoney[msg.sender] >= membersDebtBalances[msg.sender] + bills[_billID].debtAmounts[msg.sender], "not enough fund for accepting");
        bills[_billID].states[msg.sender] = BillState.ACCEPTED;
        membersDebtBalances[msg.sender] += bills[_billID].debtAmounts[msg.sender];
        bills[_billID].unaccpetedCount -= 1;
    }


    function fileDispute(uint[] memory _billsIDs, address _defendant) public returns (uint _disputeID) {
        require(double_state[msg.sender][_defendant] != DisputeState.UNSETTLED, "can't open a dispute against defendant twice");

        address _accuser = msg.sender;
        uint _totalCred = 0;
        uint _totalDebt = 0;

        for (uint i = 0; i < _billsIDs.length; i++) {
            require(_billsIDs[i] >= 0 && _billsIDs[i] < billsCount, "invalid bill ID");
            Bill storage bill = bills[_billsIDs[i]];
            require(bill.unaccpetedCount == 0, "Unaccpeted bill included.");

            if (bill.LenderAddr == _accuser && bill.states[_defendant] == BillState.ACCEPTED) {
                _totalCred += bill.debtAmounts[_defendant];
            } else if (bill.LenderAddr == _defendant && bill.states[_accuser] == BillState.ACCEPTED) {
                _totalDebt += bill.debtAmounts[_accuser];
            } else {
                revert("Unrelevant bill included");
            }
        }
        require(_totalCred > _totalDebt, "debts ammount for dispute is not positive");

        _disputeID = disputesCount++;
        disputes[_disputeID] = Dispute(_accuser, _defendant, now, _totalCred - _totalDebt, DisputeState.UNSETTLED, _billsIDs);
        double_state[msg.sender][_defendant] = DisputeState.UNSETTLED;
    }


    function claimFraud(uint _disputeID, uint _billID) validDispute(_disputeID) validBill(_billID) public {
        Dispute storage thisDispute = disputes[_disputeID];
        Bill storage thisBill = bills[_billID];
        require (thisBill.unaccpetedCount == 0, "unaccepted bill is not valid.");

        for (uint i = 0; i < thisDispute.billsIDs.length; i++) {
            if (_billID == thisDispute.billsIDs[i]) {
                revert("existing bill in dispute");
            }
        }

        if (thisDispute.accuser == thisBill.LenderAddr &&
            thisBill.states[thisDispute.defendant] == BillState.ACCEPTED) {
                thisDispute.state = DisputeState.CANCELED;
        } else if (thisDispute.defendant == thisBill.LenderAddr &&
                   thisBill.states[thisDispute.accuser] == BillState.ACCEPTED) {
            thisDispute.state = DisputeState.CANCELED;
        } else {
            revert("not related bill claimed");
        }
        double_state[thisDispute.accuser][thisDispute.defendant] = DisputeState.CANCELED;
    }


    function settleDispute(uint _disputeID) validDispute(_disputeID) public {
        require(msg.sender == disputes[_disputeID].accuser, "only accuser can settle a dispute");
        require(disputes[_disputeID].createdAt + timeout < now, "dispute is not yet timed out");

        address[] memory _borrower = new address[](1);
        _borrower[0] = disputes[_disputeID].accuser;

        disputes[_disputeID].state = DisputeState.SETTLED;
        double_state[msg.sender][disputes[_disputeID].defendant] = DisputeState.SETTLED;

        uint billID = billsCount++;
        bills[billID] = Bill("forced settlement", disputes[_disputeID].defendant, 0, _borrower);
        bills[billID].debtAmounts[disputes[_disputeID].accuser] = disputes[_disputeID].totalAmount;
        bills[billID].states[disputes[_disputeID].accuser] = BillState.ACCEPTED;

        membersDebtBalances[disputes[_disputeID].defendant] -= disputes[_disputeID].totalAmount;
        membersHoldMoney[disputes[_disputeID].defendant] -= disputes[_disputeID].totalAmount;
        membersHoldMoney[disputes[_disputeID].accuser] += disputes[_disputeID].totalAmount;
    }


    function withdraw() public payable {
        require(membersHoldMoney[msg.sender] > membersDebtBalances[msg.sender], "not enough funds");
        // uint _diff = membersHoldMoney[msg.sender] - membersDebtBalances[msg.sender];
        require(msg.sender.send(membersHoldMoney[msg.sender] - membersDebtBalances[msg.sender]));
        emit Withdraw(msg.sender, membersHoldMoney[msg.sender] - membersDebtBalances[msg.sender]);
        membersHoldMoney[msg.sender] = membersDebtBalances[msg.sender];
    }

    function getContractBalance() public view returns (uint){
        return address(this).balance;
    }
}
