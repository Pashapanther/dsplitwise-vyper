module.exports = {
  mocha: {
    reporter: 'eth-gas-reporter',
    reporterOptions : {
      currency: 'USD',
      gasPrice: 0
    }
  },
  networks: {
    development: {
      host: "127.0.0.1",
      port: 7545,
      gas: 67219750,
      network_id: "*",

    }
  }
};
