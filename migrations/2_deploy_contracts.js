var SolTest = artifacts.require("SolTest");
var VypTest = artifacts.require("VypTest");
var DSplitwiseVy = artifacts.require("DSplitwiseVy");
var SolTestDynamic = artifacts.require("SolTestDynamic");
var DSplitwise = artifacts.require("DSplitwise");
var DSplitwiseStatic = artifacts.require("DSplitwiseStatic")

module.exports = function(deployer) {
  deployer.deploy(SolTest);
  deployer.deploy(VypTest);
  deployer.deploy(SolTestDynamic);
  deployer.deploy(DSplitwise);
  deployer.deploy(DSplitwiseStatic);
  deployer.deploy(DSplitwiseVy);
};
