const utils = require('./utils');
const DSplitwiseVy = artifacts.require("DSplitwiseVy");

const BILL_ACCEPTED = 1;
const BILL_PENDING = 2;

const DISPUTE_UNSETTLED = 1;
const DISPUTE_SETTLED = 2;
const DISPUTE_CANCELED = 3;

var bill_name = "bill_123";
var bill_name_bytearray = [];
var arr = utils.randomIntArray(32);

for (var i = 0; i < 50; ++i) {
    var code;
    if (i < bill_name.length)
        code = bill_name.charCodeAt(i);
    else
        code = 0;
   bill_name_bytearray[i] = code;
}

contract("DSplitwiseVy", (accounts) => {
    before(async () => {
        this.contract = await DSplitwiseVy.deployed()
    })

    it('deploys succesfull', async () => {
        const address = await this.contract.address
        assert.notEqual(address, 0x0)
        assert.notEqual(address, '')
        assert.notEqual(address, null)
        assert.notEqual(address, undefined)
    })

    it('check initial state ', async () => {
        const billsCount = (await this.contract.bills_count()).toNumber()
        const disputesCount = (await this.contract.disputes_count()).toNumber()
        assert.equal(billsCount, 0)
        assert.equal(disputesCount, 0)
        // for (var i = 0; i < accounts.length; i++) {
        //     const address = accounts[i]
        //     assert.equal((await this.contract.debts(address)).toNumber(), 0)
        //     assert.equal((await this.contract.holds(address)).toNumber(), 0)
        //     assert.equal((await this.contract.get_balance(address)).toNumber(), 0)
        // }
    })

    it('add balance', async() => {
        for (var i = 0; i < accounts.length; i++)
            await this.contract.add_balance({from: accounts[i], value: 100*(i+1)})
        for (var i = 0; i < accounts.length; i++) {
            const address = accounts[i]
            assert.equal((await this.contract.debts(address)).toNumber(), 0)
            assert.equal((await this.contract.holds(address)).toNumber(), 100 * (i+1))
            // assert.equal((await this.contract.get_balance(address)).toNumber(), 100 * (i+1))
        }

        await this.contract.add_balance({from: accounts[0], value: 50})
        await this.contract.add_balance({from: accounts[1], value: 200})

        // assert.equal((await this.contract.get_balance(accounts[0])).toNumber(), 150)
        // assert.equal((await this.contract.get_balance(accounts[1])).toNumber(), 400)
        // assert.equal((await this.contract.get_balance(accounts[2])).toNumber(), 300)

    })

    it('withdraw', async() => {
        const address = accounts[9]

        await this.contract.withdraw({from: address})

        assert.equal((await this.contract.debts(address)).toNumber(), 0)
        assert.equal((await this.contract.holds(address)).toNumber(), 0)
        // assert.equal((await this.contract.get_balance(address)).toNumber(), 0)
    })

    it('check bills', async() => {
        await this.contract.add_bill('a', [accounts[1], accounts[2], accounts[2], accounts[2]], [20 ,5, 0, 0], 2, {from: accounts[0]})
        await this.contract.add_bill('a', [accounts[1], accounts[2], accounts[2], accounts[2]], [20 ,5, 0, 0], 2, {from: accounts[0]})
        await this.contract.add_bill('a', [accounts[1], accounts[2], accounts[2], accounts[2]], [20 ,5, 0, 0], 2, {from: accounts[0]})

        const billsCount = (await this.contract.bills_count()).toNumber()
        assert(billsCount, 3)

        await this.contract.accept_bill(2, {from: accounts[2]})
        await this.contract.accept_bill(3, {from: accounts[2]})
        await this.contract.accept_bill(3, {from: accounts[1]})

        assert.equal((await this.contract.holds(accounts[0])).toNumber(), 150)
        assert.equal((await this.contract.debts(accounts[0])).toNumber(), 0)

        assert.equal((await this.contract.holds(accounts[1])).toNumber(), 400)
        assert.equal((await this.contract.debts(accounts[1])).toNumber(), 20)

        assert.equal((await this.contract.holds(accounts[2])).toNumber(), 300)
        assert.equal((await this.contract.debts(accounts[2])).toNumber(), 10)


        for (var i = 1; i <= 3; i++) {
            // assert.equal((await this.contract.bills[i])[0], bill_name_bytearray)
            a = (await this.contract.bills)
            assert.equal(a(0), accounts[0])
            assert.equal((await this.contract.bills)[1].toNumber(), 2 - (i-1))
            // const borrowers = (await this.contract.getBillBorrowers(i))
            // assert.equal(borrowers[0], accounts[1])
            // assert.equal(borrowers[1], accounts[2])

            assert.equal((await this.contract.get_borrower_debt(i, 0)).toNumber(), 20);
            assert.equal((await this.contract.get_borrower_debt(i, 1)).toNumber(), 5);
        }

        assert.equal((await this.contract.get_borrower_state(1, 0)).toNumber(), BILL_PENDING);
        assert.equal((await this.contract.get_borrower_state(1, 1)).toNumber(), BILL_PENDING);

        assert.equal((await this.contract.get_borrower_state(2, 0)).toNumber(), BILL_PENDING);
        assert.equal((await this.contract.get_borrower_state(2, 1)).toNumber(), BILL_ACCEPTED);

        assert.equal((await this.contract.get_borrower_state(3, 0)).toNumber(), BILL_ACCEPTED);
        assert.equal((await this.contract.get_borrower_state(3, 1)).toNumber(), BILL_ACCEPTED);
    })

    it('check disputes', async() => {

        await this.contract.add_bill('a', [accounts[2], accounts[3], accounts[2], accounts[2]], [20 ,5, 0, 0], 2, {from: accounts[0]})
        await this.contract.add_bill('a', [accounts[2], accounts[3], accounts[2], accounts[2]], [20 ,5, 0, 0], 2, {from: accounts[0]})

        await this.contract.accept_bill(4, {from: accounts[2]})
        await this.contract.accept_bill(4, {from: accounts[3]})
        await this.contract.accept_bill(5, {from: accounts[2]})
        await this.contract.accept_bill(5, {from: accounts[3]})

        arr[0] = 4
        arr[1] = 5

        await this.contract.file_dispute(arr, 2, accounts[2], {from: accounts[0]})

        assert.equal(await this.contract.disputes_count(), 1)
        assert.equal((await this.contract.disputes(1))[0], accounts[0])
        assert.equal((await this.contract.disputes(1))[1], accounts[2])
        assert.equal((await this.contract.disputes(1))[3].toNumber(), 40)
        assert.equal((await this.contract.disputes(1))[4].toNumber(), DISPUTE_UNSETTLED)

        await this.contract.claim_fraud(1, 3, {from: accounts[2]})

        // await new Promise(resolve => setTimeout(resolve, 30000))
        //
        // await this.contract.settle_dispute(1, {from: accounts[0]})
        // assert.equal((await this.contract.disputes(1))[4].toNumber(), DISPUTE_SETTLED)
        //
        //
        // arr[0] = 4
        // arr[1] = 5
        // await this.contract.file_dispute(arr, 2, accounts[2], {from: accounts[0]})
        //
        // assert.equal(await this.contract.disputes_count(), 2)
        // assert.equal((await this.contract.disputes(2))[0], accounts[0])
        // assert.equal((await this.contract.disputes(2))[1], accounts[2])
        // assert.equal((await this.contract.disputes(2))[3].toNumber(), 40)
        // assert.equal((await this.contract.disputes(2))[4].toNumber(), DISPUTE_UNSETTLED)
        //
        // await this.contract.claim_fraud(2, 6, {from: accounts[2]})


    })

})
