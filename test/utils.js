function randomInt(low, high) {
  return Math.floor(Math.random() * (high - low) + low)
}

function randomIntArray(len) {
  var numbers = new Array(len)
  for (var i = 0; i < numbers.length; i++) {
    numbers[i] = randomInt(1, 1000)
  }
  return numbers
}


module.exports = {
  randomIntArray
};
