const utils = require('./utils');
const SolTest = artifacts.require("./SolTest");

contract("SolTest", (accounts) => {
  before(async () => {
    this.contract = await SolTest.deployed()
  })

  it('deploys successfully', async () => {
    const address = await this.contract.address
    assert.notEqual(address, 0x0)
    assert.notEqual(address, '')
    assert.notEqual(address, null)
    assert.notEqual(address, undefined)
  })

  for (var i = 0; i < 2; i++) {
    it('set array4', async () => {
      _array = utils.randomIntArray(4)
      await this.contract.set_array4(_array)
      const array4 = await this.contract.array4(0)
      assert.equal(array4, _array[0])
    })

    it('set array16', async () => {
      _array = utils.randomIntArray(16)
      await this.contract.set_array16(_array)
      const array4 = await this.contract.array16(0)
      assert.equal(array4, _array[0])
    })

    it('set array64', async () => {
      _array = utils.randomIntArray(64)
      await this.contract.set_array64(_array)
      const array4 = await this.contract.array64(0)
      assert.equal(array4, _array[0])
    })

    it('set array256', async () => {
      _array = utils.randomIntArray(256)
      await this.contract.set_array256(_array)
      const array4 = await this.contract.array256(0)
      assert.equal(array4, _array[0])
    })

    it('loop128', async () => {
      await this.contract.loop_128()
      assert.equal(1, 1)
    })

    it('loop256', async () => {
      await this.contract.loop_256()
      assert.equal(1, 1)
    })

    it('loop512', async () => {
      await this.contract.loop_512()
      assert.equal(1, 1)
    })

    it('add_map', async () => {
      _array = utils.randomIntArray(2)
      await this.contract.add_map(_array[0], _array[1])
      assert.equal(1, 1)
    })

    it('set_struct', async () => {
      _array = utils.randomIntArray(2)
      await this.contract.set_struct(_array[0], _array[1])
      assert.equal(1, 1)
    })
  }
})
