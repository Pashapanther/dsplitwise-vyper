const DSplitwise = artifacts.require("DSplitwise");

const BILL_ACCEPTED = 1;
const BILL_PENDING = 2;

const DISPUTE_UNSETTLED = 1;
const DISPUTE_SETTLED = 2;
const DISPUTE_CANCELED = 3;

contract("DSplitwise", (accounts) => {
    before(async () => {
        this.contract = await DSplitwise.deployed()
    })

    it('deploys succesfull', async () => {
        const address = await this.contract.address
        assert.notEqual(address, 0x0)
        assert.notEqual(address, '')
        assert.notEqual(address, null)
        assert.notEqual(address, undefined)
    })

    it('check initial state ', async () => {
        const billsCount = (await this.contract.billsCount()).toNumber()
        const disputesCount = (await this.contract.disputesCount()).toNumber()
        assert.equal(billsCount, 0)
        assert.equal(disputesCount, 0)
        for (var i = 0; i < accounts.length; i++) {
            const address = accounts[i]
            assert.equal((await this.contract.membersDebtBalances(address)).toNumber(), 0)
            assert.equal((await this.contract.membersHoldMoney(address)).toNumber(), 0)
            assert.equal((await this.contract.getBalance(address)).toNumber(), 0)
        }
    })

    it('feed some balances', async() => {
        for (var i = 0; i < accounts.length; i++)
            await this.contract.addBalance({from: accounts[i], value: 100*(i+1)})
        for (var i = 0; i < accounts.length; i++) {
            const address = accounts[i]
            assert.equal((await this.contract.membersDebtBalances(address)).toNumber(), 0)
            assert.equal((await this.contract.membersHoldMoney(address)).toNumber(), 100 * (i+1))
            assert.equal((await this.contract.getBalance(address)).toNumber(), 100 * (i+1))
        }

        await this.contract.addBalance({from: accounts[0], value: 50})
        await this.contract.addBalance({from: accounts[1], value: 200})

        assert.equal((await this.contract.getBalance(accounts[0])).toNumber(), 150)
        assert.equal((await this.contract.getBalance(accounts[1])).toNumber(), 400)
        assert.equal((await this.contract.getBalance(accounts[2])).toNumber(), 300)

    })

    it('withdraw', async() => {
        const address = accounts[9]

        await this.contract.withdraw({from: address})

        assert.equal((await this.contract.membersDebtBalances(address)).toNumber(), 0)
        assert.equal((await this.contract.membersHoldMoney(address)).toNumber(), 0)
        assert.equal((await this.contract.getBalance(address)).toNumber(), 0)
    })

    it('check bills', async() => {
        await this.contract.createBill("bill_123", [accounts[1], accounts[2]], [20 ,5], {from: accounts[0]})
        await this.contract.createBill("bill_123", [accounts[1], accounts[2]], [20 ,5], {from: accounts[0]})
        await this.contract.createBill("bill_123", [accounts[1], accounts[2]], [20 ,5], {from: accounts[0]})

        const billsCount = (await this.contract.billsCount()).toNumber()
        assert(billsCount, 3)

        await this.contract.acceptBill(1, {from: accounts[2]})
        await this.contract.acceptBill(2, {from: accounts[2]})
        await this.contract.acceptBill(2, {from: accounts[1]})

        assert.equal((await this.contract.membersHoldMoney(accounts[0])).toNumber(), 150)
        assert.equal((await this.contract.membersDebtBalances(accounts[0])).toNumber(), 0)

        assert.equal((await this.contract.membersHoldMoney(accounts[1])).toNumber(), 400)
        assert.equal((await this.contract.membersDebtBalances(accounts[1])).toNumber(), 20)

        assert.equal((await this.contract.membersHoldMoney(accounts[2])).toNumber(), 300)
        assert.equal((await this.contract.membersDebtBalances(accounts[2])).toNumber(), 10)


        for (var i = 0; i < 3; i++) {
            assert.equal((await this.contract.bills(i))[0], "bill_123")
            assert.equal((await this.contract.bills(i))[1], accounts[0])
            assert.equal((await this.contract.bills(i))[2].toNumber(), 2 - i)
            const borrowers = (await this.contract.getBillBorrowers(i))
            assert.equal(borrowers[0], accounts[1])
            assert.equal(borrowers[1], accounts[2])

            assert.equal((await this.contract.getBillBorrowerDebt(i, accounts[1])).toNumber(), 20);
            assert.equal((await this.contract.getBillBorrowerDebt(i, accounts[2])).toNumber(), 5);
        }

        assert.equal((await this.contract.getBillBorrowerState(0, accounts[1])).toNumber(), BILL_PENDING);
        assert.equal((await this.contract.getBillBorrowerState(0, accounts[2])).toNumber(), BILL_PENDING);

        assert.equal((await this.contract.getBillBorrowerState(1, accounts[1])).toNumber(), BILL_PENDING);
        assert.equal((await this.contract.getBillBorrowerState(1, accounts[2])).toNumber(), BILL_ACCEPTED);

        assert.equal((await this.contract.getBillBorrowerState(2, accounts[1])).toNumber(), BILL_ACCEPTED);
        assert.equal((await this.contract.getBillBorrowerState(2, accounts[2])).toNumber(), BILL_ACCEPTED);
    })

    it('check disputes', async() => {

        await this.contract.createBill("bill_123", [accounts[1], accounts[3]], [20 ,5], {from: accounts[0]})
        await this.contract.createBill("bill_123", [accounts[2], accounts[3]], [20 ,5], {from: accounts[0]})

        await this.contract.acceptBill(3, {from: accounts[1]})
        await this.contract.acceptBill(3, {from: accounts[3]})
        await this.contract.acceptBill(4, {from: accounts[3]})
        await this.contract.acceptBill(4, {from: accounts[2]})

        await this.contract.fileDispute([2,4], accounts[2], {from: accounts[0]})

        assert.equal((await this.contract.disputes(0))[0], accounts[0])
        assert.equal((await this.contract.disputes(0))[1], accounts[2])
        assert.equal((await this.contract.disputes(0))[3].toNumber(), 25)
        assert.equal((await this.contract.disputes(0))[4].toNumber(), DISPUTE_UNSETTLED)

        const bills = (await this.contract.getDisputeBills(0))
        assert.equal(bills[0].toNumber(), 2)
        assert.equal(bills[1].toNumber(), 4)

        var err;
        try {
            await this.contract.claimFraud(0, 1, {from: accounts[2]})
            err=true
        } catch (e) {
            err=false
        } finally {
            if(err)
                assert.fail()
        }

        try{
            await this.contract.claimFraud(0, 3, {from: accounts[2]})
            err=true
        } catch (e) {
            err=false
        } finally {
            if(err)
                assert.fail()
        }

        try{
            await this.contract.claimFraud(0, 2, {from: accounts[2]})
            err=true
        } catch (e) {
            err=false
        } finally {
            if(err)
                assert.fail()
        }

        try{
            await this.contract.fileDispute([2,3], accounts[2], {from: accounts[0]})
            err=true
        } catch (e) {
            err=false
        } finally {
            if(err)
                assert.fail()
        }

        try{
            await this.contract.fileDispute([2,4], accounts[2], {from: accounts[0]})
            err=true
        } catch (e) {
            err=false
        } finally {
            if(err)
                assert.fail()
        }

        try{
            await this.contract.fileDispute([2,4], accounts[0], {from: accounts[2]})
            err=true
        } catch (e) {
            err=false
        } finally {
            if(err)
                assert.fail()
        }


        await this.contract.acceptBill(1, {from: accounts[1]})
        await this.contract.claimFraud(0, 1, {from: accounts[2]})
        assert.equal((await this.contract.disputes(0))[4].toNumber(), DISPUTE_CANCELED)

        await this.contract.fileDispute([3,4], accounts[3], {from: accounts[0]})
        assert.equal((await this.contract.disputes(1))[4].toNumber(), DISPUTE_UNSETTLED)

        try{
            await this.contract.settleDispute(1, {from: accounts[0]})
            err=true
        } catch (e) {
            err=false
        } finally {
            if(err)
                assert.fail()
        }

        try{
            await this.contract.settleDispute(1, {from: accounts[1]})
            err=true
        } catch (e) {
            err=false
        } finally {
            if(err)
                assert.fail()
        }

        try{
            await this.contract.settleDispute(0, {from: accounts[0]})
            err=true
        } catch (e) {
            err=false
        } finally {
            if(err)
                assert.fail()
        }

        await new Promise(resolve => setTimeout(resolve, 30000))

        await this.contract.settleDispute(1, {from: accounts[0]})
        assert.equal((await this.contract.disputes(1))[4].toNumber(), DISPUTE_SETTLED)


    })

})