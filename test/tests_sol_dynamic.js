const utils = require('./utils');
const SolTestDynamic = artifacts.require("./SolTestDynamic");

contract("SolTestDynamic", (accounts) => {
  before(async () => {
    this.contract = await SolTestDynamic.deployed()
  })

  it('deploys successfully', async () => {
    const address = await this.contract.address
    assert.notEqual(address, 0x0)
    assert.notEqual(address, '')
    assert.notEqual(address, null)
    assert.notEqual(address, undefined)
  })

  for (var i = 0; i < 2; i++) {
    it('set array4', async () => {
      _array = utils.randomIntArray(4)
      await this.contract.set_array(_array)
      const array = await this.contract.array(0)
      assert.equal(array, _array[0])
    })

    it('set array16', async () => {
      _array = utils.randomIntArray(16)
      await this.contract.set_array(_array)
      const array = await this.contract.array(0)
      assert.equal(array, _array[0])
    })

    it('set array64', async () => {
      _array = utils.randomIntArray(64)
      await this.contract.set_array(_array)
      const array = await this.contract.array(0)
      assert.equal(array, _array[0])
    })

    it('set array256', async () => {
      _array = utils.randomIntArray(256)
      await this.contract.set_array(_array)
      const array = await this.contract.array(0)
      assert.equal(array, _array[0])
    })

    it('loop128', async () => {
      await this.contract.loop(128)
      assert.equal(1, 1)
    })

    it('loop256', async () => {
      await this.contract.loop(256)
      assert.equal(1, 1)
    })

    it('loop512', async () => {
      await this.contract.loop(512)
      assert.equal(1, 1)
    })
  }
})
