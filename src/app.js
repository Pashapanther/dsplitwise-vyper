App = {
    loading: false,
    contracts: {},
    newBorrowersCount: 0,
    mybills: [],

    load: async () => {
        // Load app...
        console.log("app loading...");
        await App.loadWeb3() // connect to blockchain
        await App.loadAccount()
        await App.loadContract()
        await App.render()
    },

    // https://medium.com/metamask/https-medium-com-metamask-breaking-change-injecting-web3-7722797916a8
    loadWeb3: async () => {
        if (typeof web3 !== 'undefined') {
            App.web3Provider = web3.currentProvider
            web3 = new Web3(web3.currentProvider)
        } else {
            window.alert("Please connect to Metamask.")
        }
        // Modern dapp browsers...
        if (window.ethereum) {
            window.web3 = new Web3(ethereum)
            try {
                // Request account access if needed
                await ethereum.enable()
                // Acccounts now exposed
                web3.eth.sendTransaction({/* ... */})
            } catch (error) {
                // User denied account access...
            }
        }
        // Legacy dapp browsers...
        else if (window.web3) {
            App.web3Provider = web3.currentProvider
            window.web3 = new Web3(web3.currentProvider)
            // Acccounts always exposed
            web3.eth.sendTransaction({/* ... */})
        }
        // Non-dapp browsers...
        else {
            console.log('Non-Ethereum browser detected. You should consider trying MetaMask!')
        }
    },

    loadAccount: async () => {
        App.account = web3.eth.accounts[0]
        // console.log(App.account)
    },

    loadContract: async () => {
        const dSplitwise = await $.getJSON("DSplitwise.json")
        App.contracts.DSplitwise = TruffleContract(dSplitwise)
        App.contracts.DSplitwise.setProvider(App.web3Provider)
        console.log(dSplitwise)

        // Hydrate the smart contract with values from the blockchain
        App.dSplitwise = await App.contracts.DSplitwise.deployed()
    },

    render: async () => {

        // Prevent double render
        if (App.loading) {
            return
        }

        // Update app loading state
        App.setLoading(true)

        // Render Account
        $('#account').html(App.account)
        // $('#balance').html(App)

        // Render Tasks
        await App.renderBalances()
        await App.renderBills()
        App.newBorrowersCount = 0

        // Update app loading state
        App.setLoading(false)

    },

    renderBalances: async () => {
        $('#your-hold-money').html((await App.dSplitwise.membersHoldMoney(App.account)) + " wei")
        $('#your-debts').html((await App.dSplitwise.membersDebtBalances(App.account)) + " wei")
        $('#your-balance').html((await App.dSplitwise.getBalance(App.account)) + " wei")
    },

    addBalance: async () => {
        App.setLoading(true)
        const newBalance = parseInt($('#new-add-balance').val(), 10)
        await App.dSplitwise.addBalance({value: newBalance})
        window.location.reload()
    },

    getBalance: async () => {
        App.setLoading(true)
        const b = (await App.dSplitwise.getBalance($('#get-user-balance').val())).toNumber()
        $('#user-addr').html($('#get-user-balance').val())
        $('#user-balance').html(b + " wei")
        App.setLoading(false)
        // window.location.reload()
    },

    withdraw: async () => {
        App.setLoading(true)
        await App.dSplitwise.withdraw()
        window.location.reload()
    },

    renderBills: async () => {
        // load the total task count from the blockchain
        const billsCount = await App.dSplitwise.billsCount()
        const $billTemplate = $('.billTemplate')

        // render out each task with a new task template
        var totalTotal = 0
        var row = 1;
        for (var i = 0; i < billsCount; i++) {
            const bill = await App.dSplitwise.bills(i)
            const billDescription = bill[0]
            const billLender = bill[1]
            var billCompleted = (bill[2] == 0)
            var accepted = false
            var totalAmount = 0
            var $toFrom = ""
            var show = false
            if (App.account == billLender) {
                // billCompleted = (bill[2] == 0)
                if (billCompleted)
                    App.mybills.push(i)
                const billBorrowers = await App.dSplitwise.getBillBorrowers(i)
                for (var j = 0; j < billBorrowers.length; j++) {
                    const amount = (await App.dSplitwise.getBillBorrowerDebt(i, billBorrowers[j])).toNumber()
                    totalAmount -= amount
                }
                if (billBorrowers.length > 1)
                    $toFrom = billBorrowers[0] + ", and " + (billBorrowers.length - 1) + " more"
                else
                    $toFrom = billBorrowers[0]
                show = true
            } else {
                const billBorrowers = await App.dSplitwise.getBillBorrowers(i)
                if (billBorrowers.includes(App.account)) {
                    // const billState = await App.dSplitwise.getBillBorrowerState(i, App.account)
                    totalAmount = (await App.dSplitwise.getBillBorrowerDebt(i, App.account)).toNumber()
                    $toFrom = billLender
                    const billState = (await App.dSplitwise.getBillBorrowerState(i, App.account)).toNumber()
                    accepted = (billState == 1)
                    if (billCompleted)
                        App.mybills.push(i)
                    show = true
                }
            }
            // HTML
            if (show) {
            const $newBillTemplate = $billTemplate.clone()
            $newBillTemplate.find('.dscrp').html(billDescription)
            $newBillTemplate.find('.row-num').html(row)
            $newBillTemplate.find('.bill-id').html(i)
            $newBillTemplate.find('.address').html($toFrom)
            $newBillTemplate.find('.amount').html(totalAmount)
            $newBillTemplate.find('.accept-box').find('i')
                            .addClass(App.findBillIcon(App.account == billLender, billCompleted, accepted))
                            // .off('click')
            // if (!accepted && App.account != billLender)
            //     $newBillTemplate.find('.accept-box').find('i')
            //                     .on('click', function(){App.acceptBill(i)})

            //  Put in the correct list
            $newBillTemplate.show()
            $('#billList').append($newBillTemplate)
            // console.log($('#billList'))

            // show the task
            $newBillTemplate.show()
            row += 1;
            totalTotal += totalAmount;
            }
        }
        
        const $newBillTemplate = $billTemplate.clone()
        $newBillTemplate.addClass('mdb-color lighten-5')
        $newBillTemplate.find('.dscrp').html("<b>Total Balance (debts - credits)</b>").attr('scope', 'row')
        $newBillTemplate.find('.amount').html("<b>"+totalTotal+"</b>")
        $('#billList').append($newBillTemplate)
        // show the task
        $newBillTemplate.show()


    },

    filterBills: async () => {
        App.setLoading(true)
        const addr = $('#billee').val()

        var amount = 0
        var list = []
        for (var i = 0; i < App.mybills.length; i++) {
            const element = App.mybills[i]
            const bill = await App.dSplitwise.bills(element)
            if (bill[2] != 0)
                continue
            if (bill[1] == App.account) {
                const borrowers = await App.dSplitwise.getBillBorrowers(element)
                if (borrowers.includes(addr)) {
                    amount -= (await App.dSplitwise.getBillBorrowerDebt(element, addr)).toNumber()
                    list.push(element)
                }
            } else {
                list.push(element)
                amount += (await App.dSplitwise.getBillBorrowerDebt(element, App.account)).toNumber()
            }
        }
        $('#shared-bills').html(list)
        $('#shared-balance').html(amount)
        App.setLoading(false)
    },

    createBill: async () => {
        App.setLoading(true)
        const dscrp = $('#paymentFormInputName').val()

        var baddr = []
        var bamount = []
        for (var i = 0; i < App.newBorrowersCount; i++) {
            baddr[i] = $('#baddr' + i).val()
            bamount[i] = parseInt($('#bamount' + i).val(), 10)
        }
        console.log(baddr)
        console.log(bamount)
        await App.dSplitwise.createBill(dscrp, baddr, bamount)
        window.location.reload()
    },

    acceptBill: async () => {
        App.setLoading(true)
        const id = parseInt($('#accept-id').val(), 10)
        await App.dSplitwise.acceptBill(id)
        window.location.reload()
    },

    addBorrowerField: async() => {
        const $addBorrowerTemplate = $('.borrower-template').first()
        const $newAddBorrowerTemplate = $addBorrowerTemplate.clone()
        $newAddBorrowerTemplate.find('#baddr').attr('id', "baddr"+App.newBorrowersCount)
        $newAddBorrowerTemplate.find('#baddr-label').html("Address " + (App.newBorrowersCount + 1))
                                                    .attr('for', "baddr"+App.newBorrowersCount)
        $newAddBorrowerTemplate.find('#bamount').attr('id', "bamount"+App.newBorrowersCount)
        $newAddBorrowerTemplate.find('#bamount-label').html("Amount  " + (App.newBorrowersCount + 1))
                                                    .attr('for', "bamount"+App.newBorrowersCount)
        $('#bill-form').append($newAddBorrowerTemplate)
        $newAddBorrowerTemplate.show()
        App.newBorrowersCount++

    },

    findBillIcon: (lend, completed, accept) => {
        if (completed)
            return "fa-check-square green-text disabled"
        else {
            if (lend)
                return "fa-window-close red-text disabled"
            else {
                if (accept)
                    return "fa-check-square blue-text"
                else
                    return "fa-check-square grey-text"
            }
        }
    },

    setLoading: (boolean) => {
        App.loading = boolean
        const loader = $('#loader')
        const content = $('#content')
        if (boolean) {
            loader.show()
            content.hide()
        } else {
            loader.hide()
            content.show()
        }
    }
}

$(() => {
    $(window).load(() => {
        App.load();
    })
})
